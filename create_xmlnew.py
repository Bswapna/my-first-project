from dicttoxml import dicttoxml
address = {
  "address1" : {
    "address_name" : "kurella",
    "address_street": "shivalayam",
    "address_doorno" : "2_63"
  },
	"address2" : {
    "address_name" : "miyapur",
    "address_street": "janapriyanagar",
    "address_doorno" : "9"
	}
} 

xml=dicttoxml(address)
xmlfile = open("address.xml", "w")
xmlfile.write(xml.decode())
xmlfile.close()

