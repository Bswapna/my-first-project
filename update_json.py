import json
def writing(new_data, filename='sample.json'):
    with open(filename,'r+') as file:
        file_data = json.load(file)
        file_data["employee"].append(new_data)
        file.seek(0)
        json.dump(file_data, file, indent = 4)
 
newdata = {
		"name": "mounika",
    "id": 11018,
    "phonenumber": "9948502024"
    }
     
writing(newdata)
