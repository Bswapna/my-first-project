import json

with open("sample.json") as data_file:
    data = json.load(data_file)

for element in data:
    if "sample.json"  in element:
        del element["sample.json"]

with open("sample.json", 'w') as data_file:
    data = json.dump(data, data_file)
